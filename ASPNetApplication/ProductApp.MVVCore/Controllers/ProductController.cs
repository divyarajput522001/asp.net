﻿using Microsoft.AspNetCore.Mvc;
using ProductApp.MVVCore.Models;
using ProductApp.MVVCore.Repository;

namespace ProductApp.MVVCore.Controllers
{
    public class ProductController : Controller
    {
       // List<Product> productDetails;
        readonly IProductRepsitory _productRepsitory;
        public ProductController(IProductRepsitory productRepsitory)
        {
           
            _productRepsitory = productRepsitory;
        }

        [HttpGet]
        public ActionResult Index()
        {
            List<Product> productsList = _productRepsitory.getProducts();
            
            return View(productsList);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Product product)
        {
            int productAddStatus = _productRepsitory.AddProduct(product);
            if (productAddStatus == 1)
            {

                return RedirectToAction("Index");
            }
            else
            {
                return Redirect("Create");
            }

        }

        [HttpGet]

        public ActionResult Delete(int id)
        {
            Product?  productDetailsToDelete =  _productRepsitory.DeleteProductById(id);
            return View(productDetailsToDelete);
        }
        [HttpPost]

        public ActionResult Delete(Product product)
        {
            int productDeletedStatus = _productRepsitory.DeleteProduct(product.Id);
            if (productDeletedStatus == 1)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            Product? productDetails = _productRepsitory.getProductById(id);
            if(productDetails != null)
            {
                return View(productDetails);
            }
            else
            {
                return View();
            }
        }
    }
}
