using Microsoft.EntityFrameworkCore;
using ProductApp.MVVCore.Context;
using ProductApp.MVVCore.Repository;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddScoped<IProductRepsitory, ProductRepsitory>();

string? connectionStr = "Server=LAPTOP-ULD95I2D\\SQLEXPRESS;Database=ProductSQLDb1;Trusted_Connection=True";
builder.Services.AddDbContext<ProductDbContext>(p => p.UseSqlServer(connectionStr));
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Product}/{action=Index}/{id?}");

app.Run();
