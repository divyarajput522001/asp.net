﻿using ProductApp.MVVCore.Models;

namespace ProductApp.MVVCore.Repository
{
    public interface IProductRepsitory
    {
        List<Product> getProducts();
        //bool AddProduct(Product product);
        public int AddProduct(Product product);


        Product? DeleteProductById(int id);
        // bool DeleteProduct(int id);
        public int DeleteProduct(int id);
        Product? getProductById(int id);
    }
}
